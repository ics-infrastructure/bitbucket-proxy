# -*- coding: utf-8 -*-
"""
tests.test_proxy
~~~~~~~~~~~~~~~~

This module defines the proxy tests.

:copyright: (c) 2018 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.
"""
import os
import json


JENKINS = "https://jenkins.mycompany.com"
JOB = "pipeline-job"
TOKEN = "my-secret-token"
PAYLOAD_PATH = os.path.join(os.path.abspath(os.path.dirname(__file__)), "payload")
JOB_URL = f"{JENKINS}/job/{JOB}/buildWithParameters"


def get_payload(name):
    with open(os.path.join(PAYLOAD_PATH, name)) as f:
        data = f.read()
    return json.loads(data)


def check_jenkins_params(
    qs, git_tag, git_hash, repo_url, username, git_credentials_id="bitbucket"
):
    assert qs == {
        "token": [TOKEN],
        "git_tag": [git_tag],
        "git_hash": [git_hash],
        "repo_url": [repo_url],
        "username": [username],
        "git_credentials_id": [git_credentials_id],
    }


def post(client, data, params=None, gitlab_event=None):
    if params is None:
        params = {"jenkins": JENKINS, "job": JOB, "token": TOKEN}
    if gitlab_event:
        headers = {"X-Gitlab-Event": gitlab_event}
    else:
        headers = None
    rv = client.post(
        "/build",
        query_string=params,
        data=json.dumps(data),
        content_type="application/json",
        headers=headers,
    )
    return rv


def test_index(client):
    """Test that the application returns OK on the index route"""
    rv = client.get("/")
    assert rv.data == b"OK"


def test_jenkins_parameter_missing(client, bitbucket_commit_data):
    params = {"job": JOB, "token": TOKEN}
    rv = post(client, bitbucket_commit_data, params=params)
    assert rv.status_code == 400
    assert rv.data == b"jenkins or gitlab query string parameter is required"


def test_jenkins_parameter_empty(client, bitbucket_commit_data):
    params = {"jenkins": "", "job": JOB, "token": TOKEN}
    rv = post(client, bitbucket_commit_data, params=params)
    assert rv.status_code == 400
    assert rv.data == b"Invalid jenkins or gitlab query string parameter"


def test_jenkins_domain_refused(client, bitbucket_commit_data):
    params = {"jenkins": "https://jenkins.othercompany.com", "job": JOB, "token": TOKEN}
    rv = post(client, bitbucket_commit_data, params=params)
    assert rv.status_code == 403
    assert (
        rv.data
        == b'The ciserver url "https://jenkins.othercompany.com" is not allowed.'
    )


def test_jenkins_url_with_port(client, bitbucket_commit_data):
    params = {
        "jenkins": "https://jenkins.mycompany.com:5000",
        "job": JOB,
        "token": TOKEN,
    }
    rv = post(client, bitbucket_commit_data, params=params)
    rv = post(client, bitbucket_commit_data)
    assert rv.status_code == 200


def test_bitbucket_push_one_commit(client, bitbucket_commit_data):
    rv = post(client, bitbucket_commit_data)
    assert rv.status_code == 200
    assert rv.data == b"The request was not forwarded. Build is only triggered on tags."


def test_bitbucket_push_delete_branch(client):
    data = {"push": {"changes": [{"new": None}]}}
    rv = post(client, data)
    assert rv.status_code == 200
    assert rv.data == b"The request was not forwarded. Build is only triggered on tags."


def test_bitbucket_push_one_tag(client, requests_mock):
    repo_url = "https://bitbucket.org/my_team/my_repository"
    username = "username"
    git_tag = "v1.0.1"
    git_hash = "7060238a146d04d06f39dc5ade3fcb1a79593fe8"
    # Mock the POST request to jenkins
    requests_mock.register_uri("POST", JOB_URL, status_code=201, text="Hello")
    data = {
        "push": {
            "changes": [
                {"new": {"type": "tag", "name": git_tag, "target": {"hash": git_hash}}}
            ]
        },
        "actor": {"display_name": username},
        "repository": {"links": {"html": {"href": repo_url}}},
    }
    rv = post(client, data)
    # Check that we triggered one request
    assert requests_mock.call_count == 1
    # Check the parameters sent to jenkins
    check_jenkins_params(
        requests_mock.last_request.qs, git_tag, git_hash, repo_url, username
    )
    # Check the reponse received from the mock request
    assert rv.status_code == 201
    assert rv.data == b"Hello"


def test_bitbucket_push_one_commit_and_tag(client, requests_mock):
    repo_url = "https://bitbucket.org/my_team/my_repository"
    username = "john"
    git_tag = "v1.0.2"
    git_hash = "7060238a146d04d06f39dc5ade3fcb1a79593fe8"
    # Mock the POST request to jenkins
    requests_mock.register_uri("POST", JOB_URL, status_code=201, text="")
    data = {
        "push": {
            "changes": [
                {"new": {"type": "branch", "name": "master"}},
                {"new": {"type": "tag", "name": git_tag, "target": {"hash": git_hash}}},
            ]
        },
        "actor": {"display_name": username},
        "repository": {"links": {"html": {"href": repo_url}}},
    }
    rv = post(client, data)
    # Check that we triggered one request
    assert requests_mock.call_count == 1
    # Check the parameters sent to jenkins
    check_jenkins_params(
        requests_mock.last_request.qs, git_tag, git_hash, repo_url, username
    )
    # Check the reponse received from the mock request
    assert rv.status_code == 201
    assert rv.data == b""


def test_bitbucket_push_several_tags(client, requests_mock):
    repo_url = "https://bitbucket.org/my_team/my_repository"
    username = "john"
    git_tag1 = "v2.0.1"
    git_hash1 = "qwerty"
    git_tag2 = "v2.0.2"
    git_hash2 = "asdfgh"
    git_tag3 = "v2.0.3"
    git_hash3 = "zxcvbn"
    # Mock the POST request to jenkins
    requests_mock.register_uri("POST", JOB_URL, status_code=201, text="")
    data = {
        "push": {
            "changes": [
                {"new": {"type": "branch", "name": "master"}},
                {
                    "new": {
                        "type": "tag",
                        "name": git_tag1,
                        "target": {"hash": git_hash1},
                    }
                },
                {"new": {"type": "branch", "name": "master"}},
                {
                    "new": {
                        "type": "tag",
                        "name": git_tag2,
                        "target": {"hash": git_hash2},
                    }
                },
                {
                    "new": {
                        "type": "tag",
                        "name": git_tag3,
                        "target": {"hash": git_hash3},
                    }
                },
            ]
        },
        "actor": {"display_name": username},
        "repository": {"links": {"html": {"href": repo_url}}},
    }
    rv = post(client, data)
    # Check that we triggered 3 requests
    assert requests_mock.call_count == 3
    # Check the parameters sent to jenkins
    check_jenkins_params(
        requests_mock.request_history[0].qs, git_tag1, git_hash1, repo_url, username
    )
    check_jenkins_params(
        requests_mock.request_history[1].qs, git_tag2, git_hash2, repo_url, username
    )
    check_jenkins_params(
        requests_mock.last_request.qs, git_tag3, git_hash3, repo_url, username
    )
    # Check the reponse received from the mock request
    assert rv.status_code == 201
    assert rv.data == b""


def test_bitbucket_push_real_payload(client, requests_mock):
    repo_url = (
        "https://bitbucket.org/europeanspallationsource/m-epics-plc_test_deployment"
    )
    username = "benjamin bertrand"
    # Mock the POST request to jenkins
    requests_mock.register_uri("POST", JOB_URL, status_code=201, text="Test")
    data = get_payload("bitbucket.json")
    rv = post(client, data)
    # Check that we triggered one request
    assert requests_mock.call_count == 2
    # Check the parameters sent to jenkins
    check_jenkins_params(
        requests_mock.request_history[0].qs,
        "v2.0.4",
        "20f03c87f246772d66060071b776837a0ea09260",
        repo_url,
        username,
    )
    check_jenkins_params(
        requests_mock.last_request.qs,
        "v2.0.3",
        "aeffe03fa2b140d63ff45c2cfce2680cab4ef203",
        repo_url,
        username,
    )
    # Check the reponse received from the mock request
    assert rv.status_code == 201
    assert rv.data == b"Test"


def test_bitbucket_invalid_tag(client, requests_mock):
    repo_url = "https://bitbucket.org/my_team/my_repository"
    username = "username"
    git_tag = "vendor-v2.7.7"
    git_hash = "7060238a146d04d06f39dc5ade3fcb1a79593fe8"
    # Mock the POST request to jenkins
    requests_mock.register_uri("POST", JOB_URL, status_code=201, text="")
    data = {
        "push": {
            "changes": [
                {"new": {"type": "tag", "name": git_tag, "target": {"hash": git_hash}}}
            ]
        },
        "actor": {"display_name": username},
        "repository": {"links": {"html": {"href": repo_url}}},
    }
    rv = post(client, data)
    assert rv.status_code == 200
    assert (
        rv.data == b'The request was not forwarded. "vendor-v2.7.7" is an invalid tag.'
    )
    # Check that we didn't trigger a request
    assert requests_mock.call_count == 0


def test_bitbucket_forward_last_commit_with_commit_first(client, requests_mock):
    params = {"jenkins": JENKINS, "job": JOB, "token": TOKEN, "forward": "last_commit"}
    repo_url = "https://bitbucket.org/my_team/my_repository"
    username = "guido"
    git_hash1 = "qwerty"
    git_hash2 = "asdfgh"
    git_hash3 = "zxcvbn"
    # Mock the POST request to jenkins
    requests_mock.register_uri("POST", JOB_URL, status_code=201, text="")
    data = {
        "push": {
            "changes": [
                {
                    "new": {
                        "type": "branch",
                        "name": "master",
                        "target": {"hash": git_hash1},
                    }
                },
                {
                    "new": {
                        "type": "tag",
                        "name": "v0.1.0",
                        "target": {"hash": git_hash2},
                    }
                },
                {
                    "new": {
                        "type": "branch",
                        "name": "master",
                        "target": {"hash": git_hash3},
                    }
                },
            ]
        },
        "actor": {"display_name": username},
        "repository": {"links": {"html": {"href": repo_url}}},
    }
    rv = post(client, data, params=params)
    # Check that we triggered only 1 request
    assert requests_mock.call_count == 1
    # Check the parameters sent to jenkins
    check_jenkins_params(
        requests_mock.last_request.qs, "master", git_hash1, repo_url, username
    )
    # Check the reponse received from the mock request
    assert rv.status_code == 201
    assert rv.data == b""


def test_bitbucket_forward_last_commit_with_tag_first(client, requests_mock):
    params = {"jenkins": JENKINS, "job": JOB, "token": TOKEN, "forward": "last_commit"}
    repo_url = "https://bitbucket.org/my_team/my_repository"
    username = "guido"
    git_hash1 = "qwerty"
    git_hash2 = "asdfgh"
    git_hash3 = "zxcvbn"
    # Mock the POST request to jenkins
    requests_mock.register_uri("POST", JOB_URL, status_code=201, text="")
    data = {
        "push": {
            "changes": [
                {
                    "new": {
                        "type": "tag",
                        "name": "v0.1.0",
                        "target": {"hash": git_hash1},
                    }
                },
                {
                    "new": {
                        "type": "branch",
                        "name": "master",
                        "target": {"hash": git_hash2},
                    }
                },
                {
                    "new": {
                        "type": "branch",
                        "name": "master",
                        "target": {"hash": git_hash3},
                    }
                },
            ]
        },
        "actor": {"display_name": username},
        "repository": {"links": {"html": {"href": repo_url}}},
    }
    rv = post(client, data, params=params)
    # Check that we triggered only 1 request
    assert requests_mock.call_count == 1
    # Check the parameters sent to jenkins
    check_jenkins_params(
        requests_mock.last_request.qs, "master", git_hash2, repo_url, username
    )
    # Check the reponse received from the mock request
    assert rv.status_code == 201
    assert rv.data == b""


def test_gitlab_push_one_commit_no_forward(client, gitlab_commit_data):
    rv = post(client, gitlab_commit_data, gitlab_event="Push Hook")
    assert rv.status_code == 200
    assert (
        rv.data
        == b'The "Push Hook" request was not forwarded. Build is only triggered on tags.'
    )


def test_gitlab_push_one_commit_forward(client, requests_mock):
    params = {"jenkins": JENKINS, "job": JOB, "token": TOKEN, "forward": "last_commit"}
    repo_url = "https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-and-role-test.git"
    git_hash = "3fdf1464a41a3c42405bab7408829714595cde01"
    username = "John Doe"
    data = {
        "object_kind": "push",
        "after": git_hash,
        "ref": "refs/heads/master",
        "user_name": username,
        "project": {"http_url": repo_url},
        "commits": [{"id": git_hash}],
    }
    requests_mock.register_uri("POST", JOB_URL, status_code=201, text="")
    rv = post(client, data, params=params, gitlab_event="Push Hook")
    # Check that we triggered only 1 request
    assert requests_mock.call_count == 1
    # Check the parameters sent to jenkins
    check_jenkins_params(
        requests_mock.last_request.qs,
        "master",
        git_hash,
        repo_url,
        username.lower(),
        "gitlab",
    )
    # Check the reponse received from the mock request
    assert rv.status_code == 201
    assert rv.data == b""


def test_gitlab_tag_push_real_payload(client, requests_mock):
    repo_url = "http://example.com/jsmith/example.git"
    username = "john smith"
    # Mock the POST request to jenkins
    requests_mock.register_uri("POST", JOB_URL, status_code=201, text="")
    data = get_payload("gitlab_tag_push.json")
    rv = post(client, data, gitlab_event="Tag Push Hook")
    # Check that we triggered one request
    assert requests_mock.call_count == 1
    # Check the parameters sent to jenkins
    check_jenkins_params(
        requests_mock.last_request.qs,
        "v1.0.0",
        "82b3d5ae55f7080f1e6022629cdb57bfae7cccc7",
        repo_url,
        username,
        "gitlab",
    )
    # Check the reponse received from the mock request
    assert rv.status_code == 201
    assert rv.data == b""
