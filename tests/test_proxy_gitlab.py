# -*- coding: utf-8 -*-
"""
tests.test_proxy
~~~~~~~~~~~~~~~~

This module defines the proxy tests.

:copyright: (c) 2018 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.
"""
import os
import json
import urllib.parse
from app import utils


GITLAB = "https://gitlab.mycompany.com"
PROJECT_ID = "33"
PAYLOAD_PATH = os.path.join(os.path.abspath(os.path.dirname(__file__)), "payload")
PIPELINE_URL = f"{GITLAB}/api/v4/projects/{PROJECT_ID}/trigger/pipeline"


def get_payload(name):
    with open(os.path.join(PAYLOAD_PATH, name)) as f:
        data = f.read()
    return json.loads(data)


def check_gitlab_body(body, git_tag, git_hash, repo_url, username):
    assert urllib.parse.parse_qs(body) == {
        "token": ["secret"],
        "ref": ["master"],
        "variables[REPO_URL]": [utils.get_git_url(repo_url)],
        "variables[GIT_HASH]": [git_hash],
        "variables[GIT_TAG]": [git_tag],
        "variables[USERNAME]": [username],
    }


def post(client, data, params=None, gitlab_event=None):
    if params is None:
        params = {"gitlab": GITLAB, "project_id": PROJECT_ID}
    if gitlab_event:
        headers = {"X-Gitlab-Event": gitlab_event}
    else:
        headers = None
    rv = client.post(
        "/build",
        query_string=params,
        data=json.dumps(data),
        content_type="application/json",
        headers=headers,
    )
    return rv


def test_gitlab_parameter_missing(client, bitbucket_commit_data):
    params = {"project_id": PROJECT_ID}
    rv = post(client, bitbucket_commit_data, params=params)
    assert rv.status_code == 400
    assert rv.data == b"jenkins or gitlab query string parameter is required"


def test_gitlab_parameter_empty(client, bitbucket_commit_data):
    params = {"gitlab": "", "project_id": PROJECT_ID}
    rv = post(client, bitbucket_commit_data, params=params)
    assert rv.status_code == 400
    assert rv.data == b"Invalid jenkins or gitlab query string parameter"


def test_gitlab_domain_refused(client, bitbucket_commit_data):
    params = {"gitlab": "https://gitlab.othercompany.com", "project_id": PROJECT_ID}
    rv = post(client, bitbucket_commit_data, params=params)
    assert rv.status_code == 403
    assert (
        rv.data == b'The ciserver url "https://gitlab.othercompany.com" is not allowed.'
    )


def test_bitbucket_push_one_commit(client, bitbucket_commit_data):
    rv = post(client, bitbucket_commit_data)
    assert rv.status_code == 200
    assert rv.data == b"The request was not forwarded. Build is only triggered on tags."


def test_bitbucket_push_delete_branch(client):
    data = {"push": {"changes": [{"new": None}]}}
    rv = post(client, data)
    assert rv.status_code == 200
    assert rv.data == b"The request was not forwarded. Build is only triggered on tags."


def test_bitbucket_push_one_tag(client, requests_mock):
    repo_url = "https://bitbucket.org/my_team/my_repository"
    username = "username"
    git_tag = "v1.0.1"
    git_hash = "7060238a146d04d06f39dc5ade3fcb1a79593fe8"
    # Mock the POST request to gitlab
    requests_mock.register_uri("POST", PIPELINE_URL, status_code=201, text="Hello")
    data = {
        "push": {
            "changes": [
                {"new": {"type": "tag", "name": git_tag, "target": {"hash": git_hash}}}
            ]
        },
        "actor": {"display_name": username},
        "repository": {"links": {"html": {"href": repo_url}}},
    }
    rv = post(client, data)
    # Check that we triggered one request
    assert requests_mock.call_count == 1
    # Check the parameters sent to gitlab
    check_gitlab_body(
        requests_mock.last_request.body, git_tag, git_hash, repo_url, username
    )
    # Check the reponse received from the mock request
    assert rv.status_code == 201
    assert rv.data == b"Hello"


def test_bitbucket_push_one_commit_and_tag(client, requests_mock):
    repo_url = "https://bitbucket.org/my_team/my_repository"
    username = "john"
    git_tag = "v1.0.2"
    git_hash = "7060238a146d04d06f39dc5ade3fcb1a79593fe8"
    # Mock the POST request to gitlab
    requests_mock.register_uri("POST", PIPELINE_URL, status_code=201, text="")
    data = {
        "push": {
            "changes": [
                {"new": {"type": "branch", "name": "master"}},
                {"new": {"type": "tag", "name": git_tag, "target": {"hash": git_hash}}},
            ]
        },
        "actor": {"display_name": username},
        "repository": {"links": {"html": {"href": repo_url}}},
    }
    rv = post(client, data)
    # Check that we triggered one request
    assert requests_mock.call_count == 1
    # Check the parameters sent to gitlab
    check_gitlab_body(
        requests_mock.last_request.body, git_tag, git_hash, repo_url, username
    )
    # Check the reponse received from the mock request
    assert rv.status_code == 201
    assert rv.data == b""


def test_bitbucket_push_several_tags(client, requests_mock):
    repo_url = "https://bitbucket.org/my_team/my_repository"
    username = "john"
    git_tag1 = "v2.0.1"
    git_hash1 = "qwerty"
    git_tag2 = "v2.0.2"
    git_hash2 = "asdfgh"
    git_tag3 = "v2.0.3"
    git_hash3 = "zxcvbn"
    # Mock the POST request to gitlab
    requests_mock.register_uri("POST", PIPELINE_URL, status_code=201, text="")
    data = {
        "push": {
            "changes": [
                {"new": {"type": "branch", "name": "master"}},
                {
                    "new": {
                        "type": "tag",
                        "name": git_tag1,
                        "target": {"hash": git_hash1},
                    }
                },
                {"new": {"type": "branch", "name": "master"}},
                {
                    "new": {
                        "type": "tag",
                        "name": git_tag2,
                        "target": {"hash": git_hash2},
                    }
                },
                {
                    "new": {
                        "type": "tag",
                        "name": git_tag3,
                        "target": {"hash": git_hash3},
                    }
                },
            ]
        },
        "actor": {"display_name": username},
        "repository": {"links": {"html": {"href": repo_url}}},
    }
    rv = post(client, data)
    # Check that we triggered 3 requests
    assert requests_mock.call_count == 3
    # Check the parameters sent to gitlab
    check_gitlab_body(
        requests_mock.request_history[0].body, git_tag1, git_hash1, repo_url, username
    )
    check_gitlab_body(
        requests_mock.request_history[1].body, git_tag2, git_hash2, repo_url, username
    )
    check_gitlab_body(
        requests_mock.last_request.body, git_tag3, git_hash3, repo_url, username
    )
    # Check the reponse received from the mock request
    assert rv.status_code == 201
    assert rv.data == b""


def test_bitbucket_push_real_payload(client, requests_mock):
    repo_url = (
        "https://bitbucket.org/europeanspallationsource/m-epics-plc_test_deployment"
    )
    username = "Benjamin Bertrand"
    # Mock the POST request to gitlab
    requests_mock.register_uri("POST", PIPELINE_URL, status_code=201, text="Test")
    data = get_payload("bitbucket.json")
    rv = post(client, data)
    # Check that we triggered one request
    assert requests_mock.call_count == 2
    # Check the parameters sent to gitlab
    check_gitlab_body(
        requests_mock.request_history[0].body,
        "v2.0.4",
        "20f03c87f246772d66060071b776837a0ea09260",
        repo_url,
        username,
    )
    check_gitlab_body(
        requests_mock.last_request.body,
        "v2.0.3",
        "aeffe03fa2b140d63ff45c2cfce2680cab4ef203",
        repo_url,
        username,
    )
    # Check the reponse received from the mock request
    assert rv.status_code == 201
    assert rv.data == b"Test"


def test_bitbucket_invalid_tag(client, requests_mock):
    repo_url = "https://bitbucket.org/my_team/my_repository"
    username = "username"
    git_tag = "vendor-v2.7.7"
    git_hash = "7060238a146d04d06f39dc5ade3fcb1a79593fe8"
    # Mock the POST request to gitlab
    requests_mock.register_uri("POST", PIPELINE_URL, status_code=201, text="")
    data = {
        "push": {
            "changes": [
                {"new": {"type": "tag", "name": git_tag, "target": {"hash": git_hash}}}
            ]
        },
        "actor": {"display_name": username},
        "repository": {"links": {"html": {"href": repo_url}}},
    }
    rv = post(client, data)
    assert rv.status_code == 200
    assert (
        rv.data == b'The request was not forwarded. "vendor-v2.7.7" is an invalid tag.'
    )
    # Check that we didn't trigger a request
    assert requests_mock.call_count == 0


def test_bitbucket_forward_last_commit_with_commit_first(client, requests_mock):
    params = {"gitlab": GITLAB, "project_id": PROJECT_ID, "forward": "last_commit"}
    repo_url = "https://bitbucket.org/my_team/my_repository"
    username = "guido"
    git_hash1 = "qwerty"
    git_hash2 = "asdfgh"
    git_hash3 = "zxcvbn"
    # Mock the POST request to gitlab
    requests_mock.register_uri("POST", PIPELINE_URL, status_code=201, text="")
    data = {
        "push": {
            "changes": [
                {
                    "new": {
                        "type": "branch",
                        "name": "master",
                        "target": {"hash": git_hash1},
                    }
                },
                {
                    "new": {
                        "type": "tag",
                        "name": "v0.1.0",
                        "target": {"hash": git_hash2},
                    }
                },
                {
                    "new": {
                        "type": "branch",
                        "name": "master",
                        "target": {"hash": git_hash3},
                    }
                },
            ]
        },
        "actor": {"display_name": username},
        "repository": {"links": {"html": {"href": repo_url}}},
    }
    rv = post(client, data, params=params)
    # Check that we triggered only 1 request
    assert requests_mock.call_count == 1
    # Check the parameters sent to gitlab
    check_gitlab_body(
        requests_mock.last_request.body, "master", git_hash1, repo_url, username
    )
    # Check the reponse received from the mock request
    assert rv.status_code == 201
    assert rv.data == b""


def test_bitbucket_forward_last_commit_with_tag_first(client, requests_mock):
    params = {"gitlab": GITLAB, "project_id": PROJECT_ID, "forward": "last_commit"}
    repo_url = "https://bitbucket.org/my_team/my_repository"
    username = "guido"
    git_hash1 = "qwerty"
    git_hash2 = "asdfgh"
    git_hash3 = "zxcvbn"
    # Mock the POST request to gitlab
    requests_mock.register_uri("POST", PIPELINE_URL, status_code=201, text="")
    data = {
        "push": {
            "changes": [
                {
                    "new": {
                        "type": "tag",
                        "name": "v0.1.0",
                        "target": {"hash": git_hash1},
                    }
                },
                {
                    "new": {
                        "type": "branch",
                        "name": "master",
                        "target": {"hash": git_hash2},
                    }
                },
                {
                    "new": {
                        "type": "branch",
                        "name": "master",
                        "target": {"hash": git_hash3},
                    }
                },
            ]
        },
        "actor": {"display_name": username},
        "repository": {"links": {"html": {"href": repo_url}}},
    }
    rv = post(client, data, params=params)
    # Check that we triggered only 1 request
    assert requests_mock.call_count == 1
    # Check the parameters sent to gitlab
    check_gitlab_body(
        requests_mock.last_request.body, "master", git_hash2, repo_url, username
    )
    # Check the reponse received from the mock request
    assert rv.status_code == 201
    assert rv.data == b""


def test_gitlab_push_one_commit_no_forward(client, gitlab_commit_data):
    rv = post(client, gitlab_commit_data, gitlab_event="Push Hook")
    assert rv.status_code == 200
    assert (
        rv.data
        == b'The "Push Hook" request was not forwarded. Build is only triggered on tags.'
    )


def test_gitlab_push_one_commit_forward(client, requests_mock):
    params = {"gitlab": GITLAB, "project_id": PROJECT_ID, "forward": "last_commit"}
    repo_url = "https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-and-role-test.git"
    git_hash = "3fdf1464a41a3c42405bab7408829714595cde01"
    username = "John Doe"
    data = {
        "object_kind": "push",
        "after": git_hash,
        "ref": "refs/heads/master",
        "user_name": username,
        "project": {"http_url": repo_url},
        "commits": [{"id": git_hash}],
    }
    requests_mock.register_uri("POST", PIPELINE_URL, status_code=201, text="")
    rv = post(client, data, params=params, gitlab_event="Push Hook")
    # Check that we triggered only 1 request
    assert requests_mock.call_count == 1
    # Check the parameters sent to gitlab
    check_gitlab_body(
        requests_mock.last_request.body, "master", git_hash, repo_url, username
    )
    # Check the reponse received from the mock request
    assert rv.status_code == 201
    assert rv.data == b""


def test_gitlab_tag_push_real_payload(client, requests_mock):
    repo_url = "http://example.com/jsmith/example.git"
    username = "John Smith"
    # Mock the POST request to gitlab
    requests_mock.register_uri("POST", PIPELINE_URL, status_code=201, text="")
    data = get_payload("gitlab_tag_push.json")
    rv = post(client, data, gitlab_event="Tag Push Hook")
    # Check that we triggered one request
    assert requests_mock.call_count == 1
    # Check the parameters sent to gitlab
    check_gitlab_body(
        requests_mock.last_request.body,
        "v1.0.0",
        "82b3d5ae55f7080f1e6022629cdb57bfae7cccc7",
        repo_url,
        username,
    )
    # Check the reponse received from the mock request
    assert rv.status_code == 201
    assert rv.data == b""
