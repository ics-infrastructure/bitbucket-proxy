# -*- coding: utf-8 -*-
"""
tests.test_utils
~~~~~~~~~~~~~~~~

This module defines the tests of the utils module.

:copyright: (c) 2019 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.
"""

from app import utils


def test_get_git_url():
    assert (
        utils.get_git_url("https://example.org/organization/my-repo")
        == "git@example.org:organization/my-repo.git"
    )
    assert (
        utils.get_git_url("https://example.org/organization/my-repo.git")
        == "git@example.org:organization/my-repo.git"
    )
