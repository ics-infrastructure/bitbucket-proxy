# -*- coding: utf-8 -*-
"""
app.factory
~~~~~~~~~~~

Create the WSGI application.

:copyright: (c) 2018 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import logging
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
from flask import Flask
from .main import bp as main
from . import settings


def create_app(config=None):
    app = Flask(__name__)

    app.config.from_object(settings)
    app.config.from_envvar("LOCAL_SETTINGS", silent=True)
    app.config.update(config or {})

    if app.config["SENTRY_DSN"]:
        sentry_sdk.init(dsn=app.config["SENTRY_DSN"], integrations=[FlaskIntegration()])

    if not app.debug:
        # Log to stderr by default
        handler = logging.StreamHandler()
        handler.setFormatter(
            logging.Formatter(
                "%(asctime)s %(levelname)s: %(message)s " "[in %(pathname)s:%(lineno)d]"
            )
        )
        # Set app logger level to DEBUG
        # otherwise only WARNING and above are propagated
        app.logger.setLevel(logging.DEBUG)
        handler.setLevel(logging.DEBUG)
        app.logger.addHandler(handler)
    app.logger.info("bitbucket-proxy created!")

    app.register_blueprint(main)

    return app
