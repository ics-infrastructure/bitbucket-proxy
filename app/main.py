# -*- coding: utf-8 -*-
"""
app.main
~~~~~~~~

This module implements the main blueprint.

:copyright: (c) 2018 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import requests
from collections import namedtuple
from flask import Blueprint, request, current_app
from .get_version import parse_tag
from . import utils


BuildRequest = namedtuple("BuildRequest", "ciserver job token forward data")
bp = Blueprint("main", __name__)


class BuildRequest:
    def __init__(self, request):
        try:
            self.ciserver = request.args["gitlab"]
        except KeyError:
            try:
                self.ciserver = request.args["jenkins"]
            except KeyError:
                raise utils.ProxyError(
                    "jenkins or gitlab query string parameter is required", 400
                )
            else:
                self.ci = "jenkins"
                self.job = request.args.get("job")
                self.token = request.args.get("token")
        else:
            self.ci = "gitlab"
            self.project_id = request.args.get("project_id")
        utils.validate_ciserver_url(self.ciserver)
        # By default only tags are forwarded
        # forward is an optional URL parameter that can be set to 'last_commit'
        # to forward the last commit
        # It defaults to 'tags'
        self.forward = request.args.get("forward", "tags")
        self.data = request.get_json()


@bp.app_errorhandler(utils.ProxyError)
def handle_proxy_error(error):
    return error.message, error.status_code


def trigger_gitlab_build(build_request, git_tag, git_hash, repo_url, username):
    """Send the build request to jenkins"""
    gitlab_url = f"{build_request.ciserver}/api/v4/projects/{build_request.project_id}/trigger/pipeline"
    response = requests.post(
        gitlab_url,
        data={
            "token": current_app.config["GL_TRIGGER_TOKEN"],
            "ref": "master",
            "variables[REPO_URL]": utils.get_git_url(repo_url),
            "variables[GIT_HASH]": git_hash,
            "variables[GIT_TAG]": git_tag,
            "variables[USERNAME]": username,
        },
    )
    return response


def trigger_jenkins_build(
    build_request, git_tag, git_hash, repo_url, username, git_credentials_id="bitbucket"
):
    """Send the build request to jenkins"""
    jenkins_url = (
        f"{build_request.ciserver}/job/{build_request.job}/buildWithParameters"
    )
    response = requests.post(
        jenkins_url,
        params={
            "token": build_request.token,
            "git_tag": git_tag,
            "git_hash": git_hash,
            "repo_url": repo_url,
            "git_credentials_id": git_credentials_id,
            "username": username,
        },
        auth=(
            current_app.config["JENKINS_USER_ID"],
            current_app.config["JENKINS_API_TOKEN"],
        ),
    )
    return response


def trigger_build(
    build_request, git_tag, git_hash, repo_url, username, git_credentials_id="bitbucket"
):
    """Send the build request to jenkins"""
    if build_request.ci == "gitlab":
        response = trigger_gitlab_build(
            build_request, git_tag, git_hash, repo_url, username
        )
    elif build_request.ci == "jenkins":
        response = trigger_jenkins_build(
            build_request, git_tag, git_hash, repo_url, username, git_credentials_id
        )
    current_app.logger.info(f"Request sent to {response.url}")
    current_app.logger.info(
        f"Response code: {response.status_code} / content: {response.content}"
    )
    return response


@bp.route("/build", methods=["POST"])
def build():
    """Forward the request to ciserver"""
    build_request = BuildRequest(request)
    try:
        gitlab_event = request.headers["X-Gitlab-Event"]
    except KeyError:
        # Assume bitbucket
        return bitbucket_build(build_request)
    else:
        return gitlab_build(build_request, gitlab_event)


def bitbucket_build(build_request):
    response = None
    for change in build_request.data["push"]["changes"]:
        try:
            reference_type = change["new"]["type"]
        except TypeError:
            # When a branch is deleted, new is null
            current_app.logger.debug("New reference is null. Nothing to do.")
            continue
        else:
            current_app.logger.debug(f"New reference type: {reference_type}")
        if build_request.forward == "last_commit":
            if reference_type == "tag":
                # Skip tags
                # We don't want to test twice for the same commit
                # (commit itself and tag)
                continue
            branch_name = change["new"]["name"]
            git_hash = change["new"]["target"]["hash"]
            repo_url, username = utils.get_bitbucket_info(build_request.data)
            response = trigger_build(
                build_request, branch_name, git_hash, repo_url, username
            )
            # forward only the latest commit
            # we don't want to build / run tests for all individual commits
            # when several are pushed at the same time
            break
        elif build_request.forward == "tags":
            if reference_type == "tag":
                git_tag = change["new"]["name"]
                if parse_tag(git_tag, "v") is None:
                    # We only want to build valid tags
                    # The validation is done here and not on ciserver side to avoid failure on tags
                    # that can't be built like vendor tags from upstream (m-epics-streamdevice / vendor-v2.7.7)
                    return (
                        f'The request was not forwarded. "{git_tag}" is an invalid tag.'
                    )
                git_hash = change["new"]["target"]["hash"]
                repo_url, username = utils.get_bitbucket_info(build_request.data)
                response = trigger_build(
                    build_request, git_tag, git_hash, repo_url, username
                )
    if response is None:
        return f"The request was not forwarded. Build is only triggered on {build_request.forward}."
    return (response.content, response.status_code, list(response.headers.items()))


def gitlab_build(build_request, gitlab_event):
    """Forward the gitlab request to ciserver"""
    if gitlab_event == "Push Hook":
        if build_request.forward != "last_commit":
            return f'The "Push Hook" request was not forwarded. Build is only triggered on {build_request.forward}.'
        git_hash = build_request.data["after"]
        git_ref = build_request.data["ref"].split("/", maxsplit=2)[2]
    elif gitlab_event == "Tag Push Hook":
        if build_request.forward != "tags":
            return f'The "Tag Push Hook" request was not forwarded. Build is only triggered on {build_request.forward}.'
        git_hash = build_request.data["after"]
        git_ref = build_request.data["ref"].split("/", maxsplit=2)[2]
        if parse_tag(git_ref, "v") is None:
            # We only want to build valid tags
            # The validation is done here and not on ciserver side to avoid failure on tags
            # that can't be built like vendor tags from upstream (m-epics-streamdevice / vendor-v2.7.7)
            return f'The request was not forwarded. "{git_ref}" is an invalid tag.'
    else:
        raise utils.ProxyError(f"Unknown gitlab event: {gitlab_event}")
    repo_url = build_request.data["project"]["http_url"]
    username = build_request.data["user_name"]
    response = trigger_build(
        build_request, git_ref, git_hash, repo_url, username, "gitlab"
    )
    current_app.logger.debug(response.headers.items())
    # WARNING! Sending the response.headers returned by jenkins
    # to gitlab raises an internal error (Net::ReadTimeout)
    return response.content, response.status_code


@bp.route("/")
def index():
    """Allow to check that the service is alive"""
    return "OK"
