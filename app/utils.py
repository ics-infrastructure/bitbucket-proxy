# -*- coding: utf-8 -*-
"""
app.utils
~~~~~~~~~

This module implements utility functions.

:copyright: (c) 2018 European Spallation Source ERIC
:license: BSD 2-Clause, see LICENSE for more details.

"""
import urllib.parse
from flask import current_app


class ProxyError(Exception):
    """ProxyError class

    Exception used to return HTTP status code and error message
    """

    status_code = 400

    def __init__(self, message, status_code=None):
        super().__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code

    def __str__(self):
        return self.message


def validate_ciserver_url(ciserver_url):
    ciserver_hostname = urllib.parse.urlparse(ciserver_url).hostname
    if ciserver_hostname is None:
        raise ProxyError("Invalid jenkins or gitlab query string parameter", 400)
    elif not ciserver_hostname.endswith(current_app.config["ALLOWED_CISERVER_DOMAINS"]):
        raise ProxyError(f'The ciserver url "{ciserver_url}" is not allowed.', 403)


def get_bitbucket_info(data):
    repo_url = data["repository"]["links"]["html"]["href"]
    username = data["actor"].get("display_name", "unknown")
    return (repo_url, username)


def get_git_url(url):
    """Convert a HTTPS url to git url"""
    # 1. Make sure the url doesn't end with .git
    # 2. Replace https:// with git@
    # 3. Replace the first / with :
    # 4. Add .git at the end of the url
    return (
        url.replace(".git", "").replace("https://", "git@").replace("/", ":", 1)
        + ".git"
    )
