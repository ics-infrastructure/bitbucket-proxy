import os

JENKINS_USER_ID = "username"
JENKINS_API_TOKEN = "secret_token"
GL_TRIGGER_TOKEN = os.environ.get("GL_TRIGGER_TOKEN", "secret")

# Sentry integration
# Leave to empty string to disable sentry integration
SENTRY_DSN = os.environ.get("SENTRY_DSN", "")

ALLOWED_CISERVER_DOMAINS = ("esss.lu.se",)
