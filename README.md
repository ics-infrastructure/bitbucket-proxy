# Webhook Proxy

A proxy that transforms Bitbucket and Gitlab webhooks call to a format that can trigger Jenkins parameterized builds or GitLab pipeline.
By default it only triggers a build when pushing tags.
The optional `forward` parameter can be set to 'last_commit' to forward the last commit.

This started as a fork from https://github.com/akhy/jenkins-bitbucket-webhook-proxy but was almost re-written from scratch.

## Why?

1. Bitbucket's webhook can only post **JSON** requests
2. **Change details** (e.g. commit author, commit SHA, etc) of Bitbucket's webhook calls is located **in the request body**
3. Jenkins' remotely triggered jobs (via webhook) **cannot inspect the request body**
4. We need commit details from webhook to be **available as build parameters** in Jenkins jobs
   so we can checkout and build **only those specific commits**

## Setup

### 1. Run the proxy

#### Developement

1. Clone the repository

    ```
    $ git clone https://gitlab.esss.lu.se/ics-infrastructure/bitbucket-proxy.git bitbucket-proxy
    $ cd bitbucket-proxy
    ```

2. Create a settings.cfg file with jenkins user id and API token:

    ```
    JENKINS_USER_ID = 'username'
    JENKINS_API_TOKEN = 'xxxxxxxxx'
    ```

For GitLab pipelines, define `GL_TRIGGER_TOKEN`.

There are two methods to run the proxy.

##### Running the script with Python

```
$ conda env create -n bitbucket-proxy python=3.7
$ conda activate bitbucket-proxy
(bitbucket-proxy) $ pip install -r requirements.txt
(bitbucket-proxy) $ python wsgi.py
```

##### Running with Docker

```
$ docker-compose build
$ docker-compose up
```

or:

```
$ make build
$ make test
$ make run
```

#### Production

The recommended way is to use docker:

1. Create a settings.cfg file with jenkins user id and API token and/or GitLab tokens:

    ```
    JENKINS_USER_ID = 'username'
    JENKINS_API_TOKEN = 'xxxxxxxxx'
    GL_TRIGGER_TOKEN = 'xxxxxxxxx'
    ```

    To use sentry to track errors, define the following variables:

    ```
    SENTRY_DSN = "https://<key>@sentry.io/<project>"
    ```

2. Use uwsgi to run the application:

    ```
    $ docker run -d --restart always --name bitbucket-proxy -v $(pwd)/settings.cfg:/app/settings.cfg:ro -p 8000:8000 -it bitbucket_proxy
    ```

3. You probably want to run the application behind nginx. See [flask documentation](http://flask.pocoo.org/docs/0.12/deploying/wsgi-standalone/#proxy-setups).

### 2. Setting up Bitbucket and Jenkins

#### Jenkins side

1. Open up your job configuration
2. Check **"This build is parameterized"** and add the string parameters:
   -  git_tag
   -  git_hash
   -  repo_url
   -  username
3. In build triggers section check **Trigger builds remotely** and enter any random authentication token. This token will be used in the next Bitbucket configuration.
4. Define your pipeline script. You can access as variables the string parameters passed.

#### GitLab side

- Create a project with a `.gitlab-ci.yml` file.
- Create a trigger token for this project

#### Bitbucket side

Create a webhook with **repository push** trigger and this URL template to use Jenkins:

> https://**myproxy.mycompany.com**/build?jenkins=**https://jenkins.mycompany.com:8080**&job=**job**&token=**token**

- **myproxy.mycompany.com** is where your proxy is listening
- **jenkins** query param is your Jenkins URL
- **job** query param is the Jenkins job's name (see the URL when you're editing the job)
- **token** query param is the token you have set in the Jenkins job before

Or the following to use GitLab:

> https://**myproxy.mycompany.com**/build?gitlab=**https://gitlab.mycompany.com**&project_id=**project_id**

- **myproxy.mycompany.com** is where your proxy is listening
- **gitlab** query param is your GitLab server URL
- **project_id** query param is the GitLab's project id

You can add the optional **forward** query parameter to forward the last commit (and not only tags).
Set it to 'last_commit' in that case.

Note: Currently the proxy only handles repository push trigger.

The webhook can be created using the Bitbucket API by running the **bitbucket.pex** script.
