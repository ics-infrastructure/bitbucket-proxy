FROM python:3.7-slim as base

# Install compiler to build uwsgi
FROM base as builder

RUN apt-get update \
  && apt-get install -yq --no-install-recommends \
    build-essential \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /app/requirements.txt
RUN python -m venv /venv \
  && . /venv/bin/activate \
  && pip install --no-cache-dir -r /app/requirements.txt

FROM base

MAINTAINER Benjamin Bertrand "benjamin.bertrand@esss.se"

RUN groupadd -r -g 1000 csi \
  && useradd --no-log-init -r -g csi -u 1000 csi

COPY --chown=csi:csi --from=builder /venv /venv
COPY --chown=csi:csi . /app/

WORKDIR /app

ENV PATH /venv/bin:$PATH
ENV LOCAL_SETTINGS /app/settings.cfg
ENV FLASK_APP /app/wsgi.py

EXPOSE 5000

USER csi

CMD ["uwsgi", "/app/uwsgi.ini"]
